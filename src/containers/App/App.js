import React, { Component } from 'react';
import Console from "../Console/Console";

import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Console/>
      </div>
    );
  }
}

export default App;
