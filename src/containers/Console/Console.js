import React, {Component} from 'react';
import Display from "../../comonents/Display/Display";
import {connect} from 'react-redux';

import './Console.css';
import Button from "../../comonents/Button/Button";

class Console extends Component {
  render() {
    return (
      <div className="Console">
        <Display
          back={this.props.state.access}
          display={this.props.state.displayInfo}
        />
        {this.props.state.buttons.map((button, id) => (
          <Button
            onClick={() => this.props.enterPassword(button)}
            key={id} value={button}
          />
        ))}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    state: state
  }
};

const mapDispatchToProps = dispatch => {
  return {
    enterPassword: (value) => dispatch({type: "ENTER", value: value}),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Console);
