const initialState = {
  buttons: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '<', '0', 'E'],
  factPassword: '',
  displayInfo: '',
  access: 'black'
};

const correctPassword = '3530';

const reducer = (state = initialState, action) => {

  if (action.type === 'ENTER') {
    if (action.value === '<' && state.factPassword.length > 0) {
      let factPassword = state.factPassword.split('');
      let displayInfo = state.displayInfo.split('');
      factPassword.pop();
      factPassword = factPassword.join('');
      displayInfo.pop();
      displayInfo = displayInfo.join('');

      return {
        ...state,
        factPassword: factPassword,
        displayInfo: displayInfo,
      }
    } else if (action.value === 'E' && state.factPassword.length === 4) {
      if (state.factPassword === correctPassword) {
        return {...state, displayInfo: 'Access Granted', access: 'green'}
      } else {
        return {...state, displayInfo: 'Access denied', access: 'red'}
      }
    } else if (state.factPassword.length !== 4 && action.value !== '<' && action.value !== 'E') {
      return {
      ...state,
          factPassword: state.factPassword + action.value,
          displayInfo: state.displayInfo + '*',
      }
    }
  }

  return state;
};

export default reducer;
