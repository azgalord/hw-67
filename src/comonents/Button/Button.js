import React from 'react';

import './Button.css';

const Button = ({value, onClick}) => {
  return (
    <div
      className="Button"
      onClick={onClick}
    >
      {value}
    </div>
  );
};

export default Button;
