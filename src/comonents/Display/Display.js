import React from 'react';

import './Display.css';

const Display = ({display, back}) => {
  return (
    <div style={{background: back}} className="Display">
      <p>{display}</p>
    </div>
  );
};

export default Display;
